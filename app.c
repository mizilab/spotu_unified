/***************************************************************************//**
 * @file
 * @brief Core application logic.
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include <stdio.h>

#include "em_common.h"
#include "em_gpio.h"
#include "em_system.h"
#include "em_core.h"
#include "em_adc.h"
#include "em_cryotimer.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_rmu.h"

#include "sl_app_assert.h"
#include "sl_bluetooth.h"
#include "gatt_db.h"
#include "app.h"
#include "os.h"

#include "driver/lp5036/lp5036.h"
#include "driver/da7280/da7280_hal.h"
#include "driver/max30205/max30205.h"

#include "spotu_macros.h"


#define SPOTUTASK_PRIORITY  6u
#define SPOTU_STACK_SIZE    (2000 / sizeof(CPU_STK))

#define SPOTU_EVENT_FLAG_CMD_WAITING ((OS_FLAGS)1)
#define SPOTU_EVENT_FLAG_RSP_WAITING ((OS_FLAGS)2)
#define SPOTU_EVENT_FLAG_EVT_WAITING ((OS_FLAGS)4)
#define SPOTU_EVENT_FLAG_EVT_HANDLED ((OS_FLAGS)8)


// The advertising set handle allocated from Bluetooth stack.
static uint8_t advertising_set_handle = 0xff;

OS_FLAG_GRP spotu_event_flags;
OS_MUTEX    SPOTUMutex;
static  void  SPOTUTask (void  *p_arg);
static  OS_TCB            SPOTUTaskTCB;
static  CPU_STK           SPOTUTaskStk[SPOTU_STACK_SIZE];

uint8_t SPOTU_getBoardRevision(void)
{
  uint8_t boardRevision = 0;

  if (GPIO_PinInGet(gpioPortF, 4U) == false) boardRevision |= 0x01;
  if (GPIO_PinInGet(gpioPortF, 5U) == false) boardRevision |= 0x02;
  if (GPIO_PinInGet(gpioPortF, 6U) == false) boardRevision |= 0x04;
  if (GPIO_PinInGet(gpioPortF, 7U) == false) boardRevision |= 0x08;

  return boardRevision;
}

void SPOTU_PreInit()
{
  GPIO_PinModeSet(SPOTU_FLASH_CS_PORT, SPOTU_FLASH_CS_PIN, gpioModePushPull, 1);                //FLASH
  GPIO_PinModeSet(SPOTU_ECG_CS_PORT, SPOTU_ECG_CS_PIN, gpioModePushPull, 1);                    //ECG

  GPIO_PinModeSet(SPOTU_BUTTON_PORT, SPOTU_BUTTON_PIN, gpioModeInput, 1);
  GPIO_PinModeSet(SPOTU_nSHUTDOWN_PORT, SPOTU_nSHUTDOWN_PIN, gpioModePushPull, 0);

  GPIO_PinModeSet(SPOTU_nCHARGE_PORT_OLD, SPOTU_nCHARGE_PIN_OLD, gpioModeInput, 1);
  GPIO_PinModeSet(SPOTU_nCHARGE_PORT, SPOTU_nCHARGE_PIN, gpioModeInput, 1);
  GPIO_PinModeSet(SPOTU_nLOW_BAT_PORT, SPOTU_nLOW_BAT_PIN, gpioModeInput, 0);

  GPIO_PinModeSet(SPOTU_ECG_IRQ_PORT, SPOTU_ECG_IRQ_PIN, gpioModeInput, 0);
  GPIO_PinModeSet(SPOTU_MOTION_IRQ_PORT, SPOTU_MOTION_IRQ_PIN, gpioModeInput, 0);

  GPIO_PinModeSet(SPOTU_VIBRATION_TRIG0_PORT, SPOTU_VIBRATION_TRIG0_PIN, gpioModePushPull, 0);
  GPIO_PinModeSet(SPOTU_VIBRATION_TRIG1_PORT, SPOTU_VIBRATION_TRIG1_PIN, gpioModePushPull, 0);

  //board revision pins
  GPIO_PinModeSet(gpioPortF, 4, gpioModeInputPull, 1);  //Revision bit 0
  GPIO_PinModeSet(gpioPortF, 5, gpioModeInputPull, 1);  //Revision bit 1
  GPIO_PinModeSet(gpioPortF, 6, gpioModeInputPull, 1);  //Revision bit 2
  GPIO_PinModeSet(gpioPortF, 7, gpioModeInputPull, 1);  //Revision bit 3

  //unused pins
  GPIO_PinModeSet(gpioPortD, 10, gpioModeDisabled, 0);
  GPIO_PinModeSet(gpioPortD, 11, gpioModeDisabled, 0);

  //cryotimer init.
  CMU_ClockEnable(cmuClock_CRYOTIMER, true);

  CRYOTIMER_Init_TypeDef init_cryotimer = CRYOTIMER_INIT_DEFAULT;
  init_cryotimer.osc       = cryotimerOscLFXO;    // Use the LFXO
  init_cryotimer.debugRun  = true;
  init_cryotimer.em4Wakeup = false;               // Enable EM4 wakeup upon triggering a Cryotimer interrupt
  init_cryotimer.presc     = cryotimerPresc_32;    // Set the prescaler
  init_cryotimer.period    = cryotimerPeriod_16;  // Set when wakeup events occur
  init_cryotimer.enable    = false;               // Reset the Cryotimer and don't start the timer
  CRYOTIMER_Init(&init_cryotimer);

  CRYOTIMER_IntEnable(CRYOTIMER_IEN_PERIOD);
  NVIC_EnableIRQ(CRYOTIMER_IRQn);
  CRYOTIMER_Enable(true);
}

void SPOTU_PostInit()
{
  GPIO_PinOutSet(SPOTU_nSHUTDOWN_PORT, SPOTU_nSHUTDOWN_PIN);

  /* 32.768kHz CLOCK OUT */
  GPIO_PinModeSet(SPOTU_LFXO_CLKOUT_PORT, SPOTU_LFXO_CLKOUT_PIN, gpioModePushPullAlternate, 0);

  BUS_RegMaskedWrite(&CMU->CTRL, _CMU_CTRL_CLKOUTSEL0_MASK, CMU_CTRL_CLKOUTSEL0_LFXOQ);
  BUS_RegMaskedWrite(&CMU->ROUTELOC0, _CMU_ROUTELOC0_CLKOUT0LOC_MASK, CMU_ROUTELOC0_CLKOUT0LOC_LOC4);
  BUS_RegMaskedWrite(&CMU->ROUTEPEN, _CMU_ROUTEPEN_CLKOUT0PEN_MASK, CMU_ROUTEPEN_CLKOUT0PEN);

  da7280_init();

  GPIO_PinModeSet(SPOTU_REPIRATION_ENABLE_PORT, SPOTU_REPIRATION_ENABLE_PIN, gpioModePushPull, 0);
  GPIO_PinOutSet(SPOTU_VIBRATION_TRIG1_PORT, SPOTU_VIBRATION_TRIG1_PIN);
  GPIO_PinOutClear(SPOTU_VIBRATION_TRIG1_PORT, SPOTU_VIBRATION_TRIG1_PIN);

  // ADC init
  ADC_Init_TypeDef init_adc = ADC_INIT_DEFAULT;
  ADC_InitSingle_TypeDef initSingle_adc = ADC_INITSINGLE_DEFAULT;
  init_adc.prescale = ADC_PrescaleCalc(SPOTU_ADC_FREQ, 0); // Init to max ADC clock for Series 1
  init_adc.timebase = ADC_TimebaseCalc(0);
  initSingle_adc.diff       = false;        // single ended
  initSingle_adc.reference  = adcRefVDD;    // internal 2.5V reference
  initSingle_adc.resolution = adcRes12Bit;  // 12-bit resolution
  initSingle_adc.acqTime    = adcAcqTime16;  // set acquisition time to meet minimum requirements
  initSingle_adc.posSel = adcPosSelAPORT3XCH8;
  ADC_Init(ADC0, &init_adc);
  ADC_InitSingle(ADC0, &initSingle_adc);
  ADC_Start(ADC0, adcStartSingle);

  MAX30205_init();
}

void CRYOTIMER_IRQHandler(void)
{
  // Acknowledge the interrupt
  uint32_t flags = CRYOTIMER_IntGet();
  CRYOTIMER_IntClear(flags);

  // Put a barrier here to ensure interrupts are not retriggered. See note above
  __DSB();
}


void SPOTU_CheckWakeup()
{
  uint32_t rflags;
  uint8_t buttonReleased = false;
  int i;

  //Check long key. (for 15.625ms * 128 = 2s)
  for (i = 0; i < 128; i ++)
  {
    //wait for cryotimer interrupt(for 15.625ms)
    EMU_EnterEM2(true);

    if (GPIO_PinInGet(SPOTU_BUTTON_PORT, SPOTU_BUTTON_PIN))
    {
      buttonReleased = true;
      break;
    }
  }

  if (buttonReleased) {
//  MX25_DP();
//  bhy_set_host_interface_control(BHY_HOST_AP_SUSPEND, 1);
//  max30003_sw_reset();
    MAX30205_shutdown();

    GPIO_PinOutClear(SPOTU_nSHUTDOWN_PORT, SPOTU_nSHUTDOWN_PIN);
    GPIO_PinModeSet(SPOTU_BUTTON_PORT, SPOTU_BUTTON_PIN, gpioModeInputPullFilter, 1);

    while (!GPIO_PinInGet(SPOTU_BUTTON_PORT, SPOTU_BUTTON_PIN));
    GPIO_IntClear(0x2000);
    NVIC_EnableIRQ(GPIO_EVEN_IRQn);
    GPIO_IntConfig(SPOTU_BUTTON_PORT, SPOTU_BUTTON_PIN, false, true, true);

    NVIC_EnableIRQ(GPIO_ODD_IRQn);
    GPIO_IntConfig(gpioPortB, 13U, false, true, true);

    // Enable EM4 wake up
    GPIO_EM4EnablePinWakeup(GPIO_EXTILEVEL_EM4WU4, 0);  //button
    GPIO_EM4EnablePinWakeup(GPIO_EXTILEVEL_EM4WU9, 0);  //charing
    EMU_EnterEM4S();
  }

  CRYOTIMER_Enable(false);
  CMU_ClockEnable(cmuClock_CRYOTIMER, false);
  CMU_OscillatorEnable(cmuOsc_ULFRCO, false, false);
}


/**************************************************************************//**
 * Application Init.
 *****************************************************************************/
SL_WEAK void app_init(void)
{
  RTOS_ERR os_err;

  SPOTU_PreInit();

  SPOTU_CheckWakeup();

  SPOTU_PostInit();

  OSFlagCreate(&spotu_event_flags,
               "SPOTU Flags",
               (OS_FLAGS)0,
               &os_err);
  OSMutexCreate(&SPOTUMutex,
                "SPOTU Mutex",
                &os_err);
  //create task control block for SPOTU
  OSTaskCreate(&SPOTUTaskTCB,
               "SPOTU Task",
               SPOTUTask,
               0u,
               SPOTUTASK_PRIORITY,
               &SPOTUTaskStk[0u],
               SPOTU_STACK_SIZE / 10u,
               SPOTU_STACK_SIZE,
               0u,
               0u,
               0u,
               (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               &os_err);
}

/**************************************************************************//**
 * Application Process Action.
 *****************************************************************************/
SL_WEAK void app_process_action(void)
{
  /////////////////////////////////////////////////////////////////////////////
  // Put your additional application code here!                              //
  // This is called infinitely.                                              //
  // Do not call blocking functions from here!                               //
  /////////////////////////////////////////////////////////////////////////////
}

/**************************************************************************//**
 * Bluetooth stack event handler.
 * This overrides the dummy weak implementation.
 *
 * @param[in] evt Event coming from the Bluetooth stack.
 *****************************************************************************/
void sl_bt_on_event(sl_bt_msg_t *evt)
{
  sl_status_t sc;
  bd_addr address;
  uint8_t address_type;
  char strBuf[50];
  uint8_t system_id[8];
  uint8_t board_rev;

  switch (SL_BT_MSG_ID(evt->header)) {
    // -------------------------------
    // This event indicates the device has started and the radio is ready.
    // Do not call any stack command before receiving this boot event!
    case sl_bt_evt_system_boot_id:
      // Extract unique ID from BT Address.
      sc = sl_bt_system_get_identity_address(&address, &address_type);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to get Bluetooth address\n",
                    (int)sc);

      // Pad and reverse unique ID to get System ID.
      system_id[0] = address.addr[5];
      system_id[1] = address.addr[4];
      system_id[2] = address.addr[3];
      system_id[3] = 0xFF;
      system_id[4] = 0xFE;
      system_id[5] = address.addr[2];
      system_id[6] = address.addr[1];
      system_id[7] = address.addr[0];

///////////////////////////////////////////////////////RKH////////////////////////////////////////////////////////////////
      //update broadcasting name
      sprintf(strBuf, "SPOTU_%02X%02X%02X%02X%02X%02X%02X%02X", system_id[0], system_id[1], system_id[2], system_id[3], system_id[4], system_id[5], system_id[6], system_id[7]);
      sl_bt_gatt_server_write_attribute_value(gattdb_device_name,0,strlen(strBuf),(const uint8_t*)strBuf);
      sl_bt_ota_set_device_name(22, (const uint8_t*)strBuf);

      //update firmware version
      sprintf(strBuf, "%d", __DATE_TIME_UNIX__);//__DATE__, __TIME__);
      sl_bt_gatt_server_write_attribute_value(gattdb_firmware_revision_string,0,strlen(strBuf),(const uint8_t*)strBuf);

      //update board revision
      board_rev = SPOTU_getBoardRevision();
      if (board_rev & 0x08) sprintf(strBuf, "SPOTU Lite");
      else sprintf(strBuf, "SPOTU Pro");
      sl_bt_gatt_server_write_attribute_value(gattdb_model_number_string,0,strlen(strBuf),(const uint8_t*)strBuf);

      sprintf(strBuf, "%02X", board_rev);
      sl_bt_gatt_server_write_attribute_value(gattdb_hardware_revision_string,0,strlen(strBuf),(const uint8_t*)strBuf);
///////////////////////////////////////////////////////RKH////////////////////////////////////////////////////////////////

      sc = sl_bt_gatt_server_write_attribute_value(gattdb_system_id, 0, sizeof(system_id), system_id);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to write attribute\n",
                    (int)sc);

      // Create an advertising set.
      sc = sl_bt_advertiser_create_set(&advertising_set_handle);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to create advertising set\n",
                    (int)sc);

      // Set advertising interval to 100ms.
      sc = sl_bt_advertiser_set_timing(
        advertising_set_handle,
        160, // min. adv. interval (milliseconds * 1.6)
        160, // max. adv. interval (milliseconds * 1.6)
        0,   // adv. duration
        0);  // max. num. adv. events
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to set advertising timing\n",
                    (int)sc);
      // Start general advertising and enable connections.
      sc = sl_bt_advertiser_start(advertising_set_handle, advertiser_general_discoverable, advertiser_connectable_scannable);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start advertising\n",
                    (int)sc);
      break;

    // -------------------------------
    // This event indicates that a new connection was opened.
    case sl_bt_evt_connection_opened_id:
      break;

    // -------------------------------
    // This event indicates that a connection was closed.
    case sl_bt_evt_connection_closed_id:
      // Restart advertising after client has disconnected.
      sc = sl_bt_advertiser_start(
        advertising_set_handle,
        advertiser_general_discoverable,
        advertiser_connectable_scannable);
      sl_app_assert(sc == SL_STATUS_OK,
                    "[E: 0x%04x] Failed to start advertising\n",
                    (int)sc);
      break;

    ///////////////////////////////////////////////////////////////////////////
    // Add additional event handlers here as your application requires!      //
    ///////////////////////////////////////////////////////////////////////////

    // -------------------------------
    // Default event handler.
    default:
      break;
  }
}



#define BRIGHTNESS_TABLE_FAST_MAX   8
#define BRIGHTNESS_TABLE_SLOW_MAX   16

const uint8_t brightness_table_slow[BRIGHTNESS_TABLE_SLOW_MAX] = {4, 16, 36, 64, 100, 144, 196, 255,
                                    196, 144, 100, 64, 36, 16,  4,  0};

const uint8_t brightness_table_fast[BRIGHTNESS_TABLE_FAST_MAX][4] =
    {{0,4,12,16}, {0,16,48,64}, {0,36,108,144}, {0,63,191,255},
     {0,36,108,144}, {0,16,48,64}, {0,4,12,16}, {0,0,0,0}};

void SPOTUTask(void *p_arg)
{
  (void)p_arg;

  RTOS_ERR      os_err;
  OS_FLAGS      flags = SPOTU_EVENT_FLAG_EVT_HANDLED;

  int i;
  uint8_t ledR = 0xFF;
  uint8_t ledG = 0xA0;
  uint8_t ledB = 0xB0;
  uint16_t ledInterval;

  uint8_t nvBrightnessLevel = 3;
  uint8_t currentHeartRate = 50;

  GPIO_PinModeSet(gpioPortA, 1U, gpioModePushPull, 1);
  OSTimeDlyHMSM(0, 0, 1, 0, OS_OPT_TIME_DLY | OS_OPT_TIME_HMSM_NON_STRICT, &os_err);

  lp5036_powerUp();
  lp5036_init();
  lp5036_bankControlOn();
  lp5036_bankBrightness(0);
  lp5036_bankColor(ledR, ledG, ledB);

  while (DEF_TRUE) {
    //Sample code... RKH
/*
    if (flags & SPOTU_EVENT_FLAG_CMD_WAITING) {
      flags &= ~SPOTU_EVENT_FLAG_CMD_WAITING;
      OSFlagPost(&spotu_event_flags, (OS_FLAGS)SPOTU_EVENT_FLAG_RSP_WAITING, OS_OPT_POST_FLAG_SET, &os_err);
    }

    flags |= OSFlagPend(&spotu_event_flags,
                        (OS_FLAGS)SPOTU_EVENT_FLAG_EVT_HANDLED + SPOTU_EVENT_FLAG_CMD_WAITING,
                        0,
                        OS_OPT_PEND_BLOCKING + OS_OPT_PEND_FLAG_SET_ANY + OS_OPT_PEND_FLAG_CONSUME,
                        NULL,
                        &os_err);
*/

    for(i = 0; i < BRIGHTNESS_TABLE_FAST_MAX; i++){
    lp5036_bankBrightness(brightness_table_fast[i][nvBrightnessLevel]);
            OSTimeDlyHMSM(0, 0, 0, 15, OS_OPT_TIME_DLY | OS_OPT_TIME_HMSM_NON_STRICT, &os_err);
          }

          ledInterval = 60000 / currentHeartRate;
          OSTimeDlyHMSM(0, 0, 0, ledInterval, OS_OPT_TIME_DLY | OS_OPT_TIME_HMSM_NON_STRICT, &os_err);
  }
}

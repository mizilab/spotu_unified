#include "lp5036.h"

void lp5036_reset() // return to default registers/conditions
{   
    lp5036_writeByte(LP5036_ADDRESS, LP5036_RESET, 0xFF);
}


void lp5036_powerDown()
{
    lp5036_writeByte(LP5036_ADDRESS, LP5036_DEVICE_CONFIG0, 0x00); //clear bit 6 to shut down
}


void lp5036_powerUp()
{
    lp5036_writeByte(LP5036_ADDRESS, LP5036_DEVICE_CONFIG0, 0x40); //set bit 6 to enable
}


void lp5036_init()
{
  // Bit 5 linear (0) or log scale (1)
  // Bit 4 autopower save not enabled (0) or enabled(1)
  // Bit 3 autoincrement not enabled (0) or enabled (1)
  // Bit 2 PWM dithering not enabled (0) or enabled (1)
  // Bit 1 Output max current 25.5 mA (0) or 35 mA (1)
  //
    lp5036_writeByte(LP5036_ADDRESS,LP5036_DEVICE_CONFIG1,0x20 | 0x10 | 0x08 | 0x04);// | 0x02);
}

void lp5036_bankControlOn()
{
    lp5036_writeByte(LP5036_ADDRESS,LP5036_LED_CONFIG0,0xFF);   
    lp5036_writeByte(LP5036_ADDRESS,LP5036_LED_CONFIG1,0x0F);
}

void lp5036_bankControlOff()
{
    lp5036_writeByte(LP5036_ADDRESS,LP5036_LED_CONFIG0,0x00);
    lp5036_writeByte(LP5036_ADDRESS,LP5036_LED_CONFIG1,0x00);
}


void lp5036_bankBrightness(uint8_t brightness)
{
    lp5036_writeByte(LP5036_ADDRESS,LP5036_BANK_BRIGHTNESS,brightness);
}


void lp5036_bankColor(uint8_t red, uint8_t green, uint8_t blue)
{
    lp5036_writeByte(LP5036_ADDRESS,LP5036_BANK_A_COLOR,red);
    lp5036_writeByte(LP5036_ADDRESS,LP5036_BANK_B_COLOR,green);
    lp5036_writeByte(LP5036_ADDRESS,LP5036_BANK_C_COLOR,blue);
}


void lp5036_setBrightness(uint8_t channel, uint8_t brightness)
{
    lp5036_writeByte(LP5036_ADDRESS, LP5036_LED0_BRIGHTNESS + channel, brightness); // set led brightness
}


void lp5036_setColor(uint8_t channel, uint8_t color)
{
    lp5036_writeByte(LP5036_ADDRESS, LP5036_OUT0_COLOR + channel, color);  // set color level (0 - 255) for specified led channel
}


void lp5036_setRunMode()
{
    uint8_t temp = lp5036_readByte(LP5036_ADDRESS,LP5036_DEVICE_CONFIG1);
    lp5036_writeByte(LP5036_ADDRESS,LP5036_DEVICE_CONFIG1, temp | ~(0x01) ); // normal operation 
}


void lp5036_setStopMode()
{
    uint8_t temp = lp5036_readByte(LP5036_ADDRESS,LP5036_DEVICE_CONFIG1);
    lp5036_writeByte(LP5036_ADDRESS,LP5036_DEVICE_CONFIG1, temp | 0x01 ); // shut down all leds 
}


// I2C read/write functions  

void lp5036_writeByte(uint8_t address, uint8_t subAddress, uint8_t data) {
	I2C_TransferSeq_TypeDef    seq;
	uint8_t                    i2c_read_data[2];
	uint8_t                    i2c_write_data[2];

	seq.addr  = address;
	seq.flags = I2C_FLAG_WRITE;
	/* Select command to issue */
	i2c_write_data[0] = subAddress;
	i2c_write_data[1] = data;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 2;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 0;

	I2CSPM_Transfer(I2C0, &seq);
}

uint8_t lp5036_readByte(uint8_t address, uint8_t subAddress)
{
	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[2];
	uint8_t                    i2c_write_data[2];

	seq.addr  = address;
	seq.flags = I2C_FLAG_WRITE_READ;
	/* Select command to issue */
	i2c_write_data[0] = subAddress;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 1;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 1;

	ret = I2CSPM_Transfer(I2C0, &seq);
	if (ret != i2cTransferDone) {
	    return false;
	}

	return i2c_read_data[0];
}

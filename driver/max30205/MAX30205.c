#include "MAX30205.h"
#include "os.h"
float MAX30205_temperature = 0;      // Last temperature

float MAX30205_getTemperature(void){
	uint8_t readRaw[2] = {0};
	MAX30205_I2CreadBytes(MAX30205_ADDRESS,MAX30205_TEMPERATURE, &readRaw[0] ,2); // read two bytes
	int16_t raw = readRaw[0] << 8 | readRaw[1];  //combine two bytes
    MAX30205_temperature = raw  * 0.00390625;     // convert to temperature
	return  MAX30205_temperature;
}

void MAX30205_shutdown(void){
  uint8_t reg = MAX30205_I2CreadByte(MAX30205_ADDRESS, MAX30205_CONFIGURATION);  // Get the current register
  MAX30205_I2CwriteByte(MAX30205_ADDRESS, MAX30205_CONFIGURATION, reg | 0x01);
}

void MAX30205_init(void){
  MAX30205_I2CwriteByte(MAX30205_ADDRESS, MAX30205_CONFIGURATION, 0x00); //mode config
  MAX30205_I2CwriteByte(MAX30205_ADDRESS, MAX30205_THYST , 		  0x00); //set threshold
  MAX30205_I2CwriteByte(MAX30205_ADDRESS, MAX30205_TOS, 		  0x00); //
}

void MAX30205_printRegisters(void){
  //Serial.println(I2CreadByte(MAX30205_ADDRESS, MAX30205_TEMPERATURE),  BIN);
  //Serial.println(I2CreadByte(MAX30205_ADDRESS, MAX30205_CONFIGURATION),  BIN);
  //Serial.println(I2CreadByte(MAX30205_ADDRESS, MAX30205_THYST), BIN);
  //Serial.println(I2CreadByte(MAX30205_ADDRESS, MAX30205_TOS), BIN);

}

// Wire.h read and write protocols
void MAX30205_I2CwriteByte(uint8_t address, uint8_t subAddress, uint8_t data)
{
	I2C_TransferSeq_TypeDef    seq;
	//I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[2];
	uint8_t                    i2c_write_data[2];

	seq.addr  = address;
	seq.flags = I2C_FLAG_WRITE;
	/* Select command to issue */
	i2c_write_data[0] = subAddress;
	i2c_write_data[1] = data;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 2;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 0;

	I2CSPM_Transfer(I2C1, &seq);
}

uint8_t MAX30205_I2CreadByte(uint8_t address, uint8_t subAddress)
{
	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[2];
	uint8_t                    i2c_write_data[2];

	seq.addr  = address;
	seq.flags = I2C_FLAG_WRITE_READ;
	/* Select command to issue */
	i2c_write_data[0] = subAddress;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 1;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = 1;

	ret = I2CSPM_Transfer(I2C1, &seq);
	if (ret != i2cTransferDone) {
	    return false;
	}

	return i2c_read_data[0];
}

void MAX30205_I2CreadBytes(uint8_t address, uint8_t subAddress, uint8_t *dest, uint8_t count)
{
	uint8_t stringpos = 0;
	I2C_TransferSeq_TypeDef    seq;
	I2C_TransferReturn_TypeDef ret;
	uint8_t                    i2c_read_data[2];
	uint8_t                    i2c_write_data[2];

	seq.addr  = address;
	seq.flags = I2C_FLAG_WRITE_READ;
	/* Select command to issue */
	i2c_write_data[0] = subAddress;
	seq.buf[0].data   = i2c_write_data;
	seq.buf[0].len    = 1;
	/* Select location/length of data to be read */
	seq.buf[1].data = i2c_read_data;
	seq.buf[1].len  = count;

	ret = I2CSPM_Transfer(I2C1, &seq);
	if (ret != i2cTransferDone) {
		return;
	}

	for (stringpos = 0; stringpos < count; stringpos++)
		*(dest + stringpos) = i2c_read_data[stringpos];

	return;
}


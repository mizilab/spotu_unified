/*
 * spotu_macros.h
 *
 *  Created on: 2021. 5. 13.
 *      Author: waaki
 */

#ifndef SPOTU_MACROS_H_
#define SPOTU_MACROS_H_

#define Y2K_UNIX_EPOCH_DIFF 946684800U
#define YEARS ((__DATE__[10] - '0' + (__DATE__[9] - '0') * 10))
#define DAY_OF_MONTH ((__DATE__[5] - '0') \
                  + (((__DATE__[4] > '0')? __DATE__[4] - '0': 0) * 10) - 1)
#define DAY_OF_YEAR ((DAY_OF_MONTH) + \
( /* Jan */ (__DATE__[0] == 'J' && __DATE__[1] == 'a')?   0: \
  /* Feb */ (__DATE__[0] == 'F'                      )?  31: \
  /* Mar */ (__DATE__[0] == 'M' && __DATE__[2] == 'r')?  59: \
  /* Apr */ (__DATE__[0] == 'A' && __DATE__[1] == 'p')?  90: \
  /* May */ (__DATE__[0] == 'M'                      )? 120: \
  /* Jun */ (__DATE__[0] == 'J' && __DATE__[2] == 'n')? 151: \
  /* Jul */ (__DATE__[0] == 'J'                      )? 181: \
  /* Aug */ (__DATE__[0] == 'A'                      )? 212: \
  /* Sep */ (__DATE__[0] == 'S'                      )? 243: \
  /* Oct */ (__DATE__[0] == 'O'                      )? 273: \
  /* Nov */ (__DATE__[0] == 'N'                      )? 304: \
  /* Dec */                                             334  ))
#define LEAP_DAYS (YEARS / 4 + ((YEARS % 4 == 0 && DAY_OF_YEAR > 58)? 1 : 0) )
#define __DATE_TIME_Y2K__ ( (YEARS * 365 + LEAP_DAYS + DAY_OF_YEAR ) * 86400 \
                    + ((__TIME__[0] - '0') * 10 + __TIME__[1] - '0') * 3600 \
                    + ((__TIME__[3] - '0') * 10 + __TIME__[4] - '0') * 60 \
                    + ((__TIME__[6] - '0') * 10 + __TIME__[7] - '0') )
#define  __DATE_TIME_UNIX__ ( __DATE_TIME_Y2K__ + Y2K_UNIX_EPOCH_DIFF )


#define SPOTU_FLASH_CS_PORT           (gpioPortB)
#define SPOTU_FLASH_CS_PIN            (12U)
#define SPOTU_ECG_CS_PORT             (gpioPortB)
#define SPOTU_ECG_CS_PIN              (11U)

#define SPOTU_BUTTON_PORT             (gpioPortD)
#define SPOTU_BUTTON_PIN              (14U)

#define SPOTU_nSHUTDOWN_PORT          (gpioPortA)
#define SPOTU_nSHUTDOWN_PIN           (1U)

#define SPOTU_nCHARGE_PORT_OLD        (gpioPortC)
#define SPOTU_nCHARGE_PIN_OLD         (9U)
#define SPOTU_nCHARGE_PORT            (gpioPortB)
#define SPOTU_nCHARGE_PIN             (13U)
#define SPOTU_nLOW_BAT_PORT           (gpioPortF)
#define SPOTU_nLOW_BAT_PIN            (3U)

#define SPOTU_ECG_IRQ_PORT            (gpioPortA)
#define SPOTU_ECG_IRQ_PIN             (2U)
#define SPOTU_ECG_IRQ_NO              (4U)
#define SPOTU_MOTION_IRQ_PORT         (gpioPortA)
#define SPOTU_MOTION_IRQ_PIN          (3U)
#define SPOTU_MOTION_IRQ_NO           (3U)

#define SPOTU_VIBRATION_TRIG0_PORT    (gpioPortD)
#define SPOTU_VIBRATION_TRIG0_PIN     (13U)
#define SPOTU_VIBRATION_TRIG1_PORT    (gpioPortD)
#define SPOTU_VIBRATION_TRIG1_PIN     (15U)

#define SPOTU_LFXO_CLKOUT_PORT        (gpioPortD)
#define SPOTU_LFXO_CLKOUT_PIN         (9U)

#define SPOTU_REPIRATION_ENABLE_PORT  (gpioPortD)
#define SPOTU_REPIRATION_ENABLE_PIN   (12U)

#define SPOTU_ADC_FREQ                13000000









#endif /* SPOTU_MACROS_H_ */
